namespace Logbook

module Views =
    open System
    open Giraffe.GiraffeViewEngine
    open Humanizer

    // helpers
    let private metaCharset (value: string) = meta [ _charset value ]

    let private meta (key: string) (value: string) =
        meta
            [ _name key
              _content value ]

    let baseLayout (content: XmlNode list) =
        html []
            [ head []
                  [ metaCharset "utf-8"
                    meta "viewport" "width=device-width, initial-scale=1.0"

                    // Page title
                    title [] [ encodedText "Personal Logbook" ]

                    // Favicon
                    link
                        [ _rel "icon"
                          _type "image/x-icon"
                          _href "/favicon.svg" ]

                    // CSS
                    link
                        [ _rel "stylesheet"
                          _type "text/css"
                          _href "/main.css" ] ]

              body [] content ]

    let newLogForm () =
        div []
            [ form
                [ _action "/new"
                  _method "POST" ]
                  [ textarea
                      [ _type "msg"
                        _placeholder "What's up?"
                        _name "Content"
                        _autofocus ] []
                    button [ _type "submit" ] [ encodedText "Log" ]
                    button
                        [ _type "reset"
                          _value "Reset" ] [ encodedText "Clear" ] ] ]

    let editForm log =
        [ div []
              [ form
                  [ _action (Urls.editLogByID log.Id)
                    _method "POST" ]
                    [ textarea
                        [ _type "msg"
                          _name "Content"
                          _autofocus ] [ rawText log.Content ]
                      button [ _type "submit" ] [ encodedText "Update" ] ]
                form
                    [ _action (Urls.deleteLogByID log.Id)
                      _method "POST" ] [ button [ _type "submit" ] [ encodedText "Delete" ] ] ] ]
        |> baseLayout

    let logRecord id (timestamp: DateTime) content =
        let logMetadata =
            div []
                [ span [] [ encodedText (timestamp.Humanize()) ]
                  a [ _href (Urls.editLogByID id) ] [ encodedText "EDIT" ] ]
        div [ _id (string id) ]
            [ logMetadata
              div [] [ encodedText content ] ]


    let index (logs: Log list) =
        [ h1 [] [ encodedText "Personal Logbook" ]
          newLogForm ()
          for log in logs do
              (logRecord log.Id log.Timestamp log.Content) ]
        |> baseLayout