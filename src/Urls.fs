namespace Logbook

module Urls =
    let index = "/"
    let newLogForm = "/new"
    let editLogByIDRoute: PrintfFormat<int -> obj, obj, obj, obj, int> = "/edit/%i"
    let editLogByID = sprintf "/edit/%i"
    let deleteLogByIDRoute: PrintfFormat<int -> obj, obj, obj, obj, int> = "/edit/%i/delete"
    let deleteLogByID = sprintf "/edit/%i/delete"
