create table Logs (
    Id int primary key autoincrement,
    CreationDateTime datetime,
    Content string
);

create index IX_Logs_CreationDateTime on Logs (CreationDateTime);
