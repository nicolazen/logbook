namespace Logbook

open System
open Rezoom.SQL
open Rezoom.SQL.Mapping
open Rezoom.SQL.Migrations
open Rezoom.SQL.Synchronous

module DatabaseStartup =
    type DBModel = SQLModel<".">

    let initialize () =
        DefaultConnectionProvider.SetConfigurationReader(fun connectionName ->
            match connectionName.Equals "logbook" with
            | true ->
                { ConnectionString = "Data Source=logbook.db"
                  ProviderName = "System.Data.SQLite" }
            | false -> failwithf "unknown connection name '%s'" connectionName)

    let migrate () =
        // customize the default migration config so that it outputs a message after running a migration
        let config =
            { MigrationConfig.Default with LogMigrationRan = (fun m -> printfn "Executed migration [%s]" m.MigrationName) }

        DBModel.Migrate(config)

module Database =
    module SQLStatements =
        type InsertLog = SQL<"""
            insert into Logs(CreationDateTime,Content) values (@currentDateTime, @content)
        """>

        type SelectLog = SQL<"""
            select Id, CreationDateTime, Content from Logs
            where Id = @id
        """>

        type SelectAllLogs = SQL<"""
            select Id, CreationDateTime, Content from Logs
            order by CreationDateTime desc
        """>

        type SelectLogsPaginated = SQL<"""
            select CreationDateTime, Content from Logs
            where Id > @startId
            order by CreationDateTime desc
            limit @pageSize
        """>

        type UpdateLog = SQL<"""
            update Logs
            set Content = @newContent
            where Id = @id
        """>

        type DeleteLog = SQL<"""
            delete from Logs
            where Id = @id
        """>

    let addLog content =
        use context = new ConnectionContext()
        SQLStatements.InsertLog.Command(content = content, currentDateTime = DateTime.UtcNow).Execute(context)

    let getAllLogs () =
        use context = new ConnectionContext()

        let logs =
            SQLStatements.SelectAllLogs.Command().Execute(context)

        [ for log in logs do
            { Log.Id = log.Id
              Log.Timestamp = log.CreationDateTime
              Log.Content = log.Content } ]

    let getLog id =
        use context = new ConnectionContext()

        let log =
            SQLStatements.SelectLog.Command(id = id).ExecuteExactlyOne(context)

        { Log.Id = log.Id
          Log.Timestamp = log.CreationDateTime
          Log.Content = log.Content }

    let updateLog id content =
        use context = new ConnectionContext()
        SQLStatements.UpdateLog.Command(id = id, newContent = content).Execute(context)

    let deleteLog (id: int) =
        use context = new ConnectionContext()
        SQLStatements.DeleteLog.Command(id = id).Execute(context)
