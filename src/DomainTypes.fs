namespace Logbook

[<AutoOpen>]
module DomainTypes =
    open System

    [<CLIMutable>]
    type Log =
        { Id: int
          Timestamp: DateTime
          Content: string }

    [<CLIMutable>]
    type NewLog =
        { Content: string }
