namespace Logbook

open System
open Microsoft.AspNetCore.Http
open FSharp.Control.Tasks.V2.ContextInsensitive
open Giraffe

module WebApp =
    let private allowCaching (duration: TimeSpan): HttpHandler =
        publicResponseCaching (int duration.TotalSeconds) (Some "Accept-Encoding")

    let indexHandler () =
        let view = Database.getAllLogs () |> Views.index
        htmlView view

    let newLogHandler: HttpHandler =
        fun (next: HttpFunc) (ctx: HttpContext) ->
            task {
                let! result = ctx.TryBindFormAsync<NewLog>()

                match result with
                | Error err -> return! RequestErrors.BAD_REQUEST err next ctx
                | Ok log ->
                    Database.addLog log.Content
                    return! next ctx
            }

    let editLogFormHandler (logID: int): HttpHandler =
        let log = Database.getLog logID
        let view = Views.editForm log
        htmlView view

    let editLogHandler logID: HttpHandler =
        fun (next: HttpFunc) (ctx: HttpContext) ->
            task {
                let! result = ctx.TryBindFormAsync<NewLog>()

                match result with
                | Error err -> return! RequestErrors.BAD_REQUEST err next ctx
                | Ok log ->
                    Database.updateLog logID log.Content
                    return! next ctx
            }

    let deleteLogHandler logID: HttpHandler =
        fun (next: HttpFunc) (ctx: HttpContext) ->
            task {
                Database.deleteLog logID
                return! next ctx
            }

    let app: HttpHandler =
        choose
            [ GET >=> choose
                          [ route Urls.index >=> warbler (fun _ -> indexHandler ())
                            routef Urls.editLogByIDRoute editLogFormHandler ]
              POST >=> choose
                           [ route Urls.newLogForm >=> newLogHandler >=> redirectTo true Urls.index
                             routef Urls.editLogByIDRoute editLogHandler >=> redirectTo true Urls.index
                             routef Urls.deleteLogByIDRoute deleteLogHandler >=> redirectTo true Urls.index ]
              setStatusCode 404 >=> text "Not Found" ]
